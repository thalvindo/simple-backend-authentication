const express = require("express");
const cors = require("cors");
const app = express();

app.use(cors());

app.use("/login", (req, res) => {
  res.send({
    token: "test",
  });
});

app.listen(8080, () => console.log("http://localhost:8080/login"));
// to run = `node server.js`
